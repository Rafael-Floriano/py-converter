import os
import pandas as pd
from dbfread import DBF

def get_files_ends_with_dbf():
    files_list = []
    for file in os.listdir("./"):
        if file.endswith(".dbf"):
           files_list.append(file) 
    return files_list


def check_if_folder_of_convertion_exists():
    folder_list = []
    for folder in os.listdir("./"):
        if folder == "converted-spreadsheets":
            return True
    create_folder_of_convertions_spreadsheets()

def create_folder_of_convertions_spreadsheets():
    current_path = os.getcwd()
    try: 
        print("Criando Pasta --conveted-spreadsheets--")
        os.mkdir(current_path + "/converted-spreadsheets/") 
    except OSError as error: 
        print(error)

check_if_folder_of_convertion_exists()
file_list = get_files_ends_with_dbf()

for file in file_list: 
    try:
        print("File: " + file)
        dbf_file = DBF(file, ignore_missing_memofile=False)
        df = pd.DataFrame(iter(dbf_file))
        new_name = file.split('.dbf')
        new_name = new_name[0]
        df.to_csv("./converted-spreadsheets/" + new_name)
    except OSError as error:
        print(error)
